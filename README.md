# About
I created this as I was playing with google api access at work. By no means is it complete but it does do some basic things that I needed. I thought I would share in the event someone needed to do something similar.

# Install
```
pip install git+https://hm34306@bitbucket.org/hm34306/py-gapi-client.git
```

# ex.
```
import json
from py_gapi_client import credentials
from py_gapi_client.sheets import SheetAccess, SheetQuery
from py_gapi_client.scope import FluentSheetScope

creds = credentials.OAuth2.get(FluentSheetScope().full_control().scope)
#creds = credentials.ApiToken.get()
print(creds.credential)
sheet_access = SheetAccess(creds)

sheet_query = SheetQuery("<some doc id>", "Inputs!A4:P1000")
sheet = sheet_access.get(sheet_query)
print(sheet.names)
sheet_rows = sheet_access.add_sheet("hm-sheet", sheet_query)
print(sheet_rows)
print(sheet.names)
```