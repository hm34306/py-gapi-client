from .. import AbstractGObject, AbstractGService


class AbstractGDocsObject(AbstractGObject):
    pass


class AbstractGDocsService(AbstractGService):

    @property
    def service_type(self):
        return "docs"

    @property
    def service_version(self):
        return "v1"
