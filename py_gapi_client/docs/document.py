from . import AbstractGDocsObject, AbstractGDocsService


class Document(AbstractGDocsService):
    def __init__(self, document_service, document_query, response):
        AbstractGDocsService.__init__(self, document_service, document_query, response)

    @property
    def document(self):
        return self.response


class DefaultDocument(AbstractGDocsObject):
    def __int__(self, credentials=None):
        AbstractGDocsObject.__init__(self, credentials)
        self.credentials = credentials

    def service_type(self):
        return "docs"

    def service_version(self):
        return "v1"

    @property
    def document(self, document_query, document_return_type=Document):
        response = (
            self.service.documents()
            .get(documentId=document_query.document_id)
            .execute()
        )
        return document_return_type(self.service, document_query, response)