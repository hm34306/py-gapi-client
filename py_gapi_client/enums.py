import enum


class GCredentialType(enum.Enum):
    OAUTH2 = 1
    API_TOKEN = 2
