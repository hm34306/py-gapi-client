from . import AbstractGSheetObject


class SheetMetaData(AbstractGSheetObject):
    def __init__(self, sheet_service, sheet_query, response):
        AbstractGSheetObject.__init__(self, sheet_service, sheet_query, response)
        self._sheet_metadata = None

    @property
    def sheet_metadata(self):
        if self._sheet_metadata is None:
            self._sheet_metadata = (
                self.service.spreadsheets().get(spreadsheetId=self.query.id).execute()
            )
        return self._sheet_metadata
