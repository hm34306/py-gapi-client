from .. import AbstractGService, AbstractGObject


class AbstractGSheetObject(AbstractGObject):
    pass


class AbstractGSheetService(AbstractGService):

    @property
    def service_type(self):
        return "sheets"

    @property
    def service_version(self):
        return "v4"
