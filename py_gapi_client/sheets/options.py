from abc import ABC, abstractproperty
from .enums import ValueRenderOption, DateTimeRenderOption


class AbstractSheetOptions(ABC):
    @abstractproperty
    def valueRenderOption(self):
        pass

    @abstractproperty
    def dateTimeRenderOption(self):
        pass


class DefaultSheetOptions:
    @property
    def valueRenderOption(self):
        return ValueRenderOption.FORMULA.value

    @property
    def dateTimeRenderOption(self):
        return DateTimeRenderOption.FORMATTED_STRING.value