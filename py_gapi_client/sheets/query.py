from .. import AbstractQuery
from .options import DefaultSheetOptions


class SheetQuery(AbstractQuery):
    def __init__(self, spreadsheet_id, sheet_range, sheetoptions=DefaultSheetOptions()):
        AbstractQuery.__init__(self, spreadsheet_id)
        self.range = sheet_range
        self.sheetoptions = sheetoptions
