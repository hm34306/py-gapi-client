from . import AbstractGSheetService
from .sheet import Sheet


class SpreadSheet(AbstractGSheetService):
    def __int__(self, credentials):
        AbstractGSheetService.__init__(self, credentials)
        self.credentials = credentials

    @property
    def service_type(self):
        return "sheets"

    @property
    def service_version(self):
        return "v4"

    def create(self, title):
        body = {"properties": {"title": title}}
        request = self.service.spreadsheets().create(body=body)
        sheet_response = request.execute()

        return sheet_response

    def add_sheet(self, title, sheet_query, sheet_return_type=Sheet):
        request = [{"addSheet": {"properties": {"title": title}}}]
        body = {"requests": request}

        try:
            sheet_response = (
                self.service.spreadsheets()
                .batchUpdate(spreadsheetId=sheet_query.id, body=body)
                .execute()
            )
        except Exception as ex:
            if (
                f'A sheet with the name \\"{title}\\" already exists. Please enter another name.'
                in ex.content.decode("utf-8")
            ):
                return self.sheet(sheet_query, sheet_return_type)

        return sheet_return_type(self.service, sheet_response, sheet_query)

    def sheet(self, sheet_query, sheet_return_type=Sheet):
        sheet = self.service.spreadsheets()
        sheet_response = (
            sheet.values()
            .get(
                spreadsheetId=sheet_query.id,
                range=sheet_query.range,
                valueRenderOption=sheet_query.sheetoptions.valueRenderOption,
                dateTimeRenderOption=sheet_query.sheetoptions.dateTimeRenderOption,
            )
            .execute()
        )

        return sheet_return_type(self.service, sheet_query, sheet_response)
