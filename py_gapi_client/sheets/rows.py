from .enums import ValueInputOption
from .sheets_requests import delete, insert, paste_data, values
from .metadata import SheetMetaData


class SheetRows(SheetMetaData):
    def __init__(self, service, sheet_query, sheet_rows, sheet_id):
        SheetMetaData.__init__(self, service, sheet_query, sheet_rows)
        self._sheet_id = sheet_id

    @property
    def rows(self):
        return self.response

    @property
    def name(self):
        return self.query.range.split("!")[0]

    @property
    def sheet_id(self):
        return self._sheet_id

    @rows.setter
    def rows(self, value):
        self.response = value

    def update(
        self,
        valueInputOption=ValueInputOption.USER_ENTERED.value,
        new_rows=None,
        temp_range=None,
    ):
        body = values(new_rows or self.rows)

        result = (
            self.service.spreadsheets()
            .values()
            .update(
                spreadsheetId=self.sheet_id,
                range=temp_range or self.query.range,
                valueInputOption=valueInputOption,
                body=body,
            )
            .execute()
        )

        return result

    def delete(self, start_index, end_index=None):
        if end_index is None:
            start_index = start_index - 1
            end_index = start_index

        request = [delete(self.sheet_id, start_index, end_index)]
        body = {"requests": request}
        sheet_response = (
            self.service.spreadsheets()
            .batchUpdate(spreadsheetId=self.query.id, body=body)
            .execute()
        )

        for index in range(start_index + 1, end_index):
            self.rows.pop(index)

        return self.rows

    def insert(self, start_index, end_index, row_data=None):
        if end_index is None:
            start_index = start_index - 1
            end_index = start_index

        request = [insert(self.sheet_id, start_index, end_index)]
        if row_data is not None:
            request.append(paste_data(self.sheet_id, start_index, row_data))

        body = {"requests": request}
        sheet_response = (
            self.service.spreadsheets()
            .batchUpdate(spreadsheetId=self.query.id, body=body)
            .execute()
        )

        return self.rows
