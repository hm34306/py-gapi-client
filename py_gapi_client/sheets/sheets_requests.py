def delete(sheet_id, start_index, end_index):
    return {
        "deleteDimension": {
            "range": {
                "sheetId": sheet_id,
                "dimension": "ROWS",
                "startIndex": start_index,
                "endIndex": end_index,
            }
        }
    }


def insert(sheet_id, start_index, end_index):
    return {
        "insertDimension": {
            "range": {
                "sheetId": sheet_id,
                "dimension": "ROWS",
                "startIndex": start_index,
                "endIndex": end_index,
            }
        }
    }


def paste_data(sheet_id, row_index, row_data):
    return {
        "pasteData": {
            "data": row_data,
            "type": "PASTE_NORMAL",
            "delimiter": ",",
            "coordinate": {"sheetId": sheet_id, "rowIndex": row_index},
        }
    }


def values(rows):
    return {"values": rows}
