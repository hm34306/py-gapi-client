from .rows import SheetRows
from .metadata import SheetMetaData


class Sheet(SheetMetaData):
    def __init__(self, sheet_service, sheet_query, response):
        SheetMetaData.__init__(self, sheet_service, sheet_query, response)

    @property
    def name(self):
        return self.query.range.split("!")[0]

    @property
    def sheet_id(self):
        sheets = self.sheet_metadata.get("sheets", "")
        for sheet in sheets:
            print(
                sheet
            )  # {'properties': {'sheetId': 603400104, 'title': 'hm-sheet', 'index': 1, 'sheetType': 'GRID', 'gridProperties': {'rowCount': 1000, 'columnCount': 26}}
            sheet_name = sheet.get("properties").get("sheetId")
            if sheet_name == self.name:
                return sheet.get("properties").get("sheetId")

        return sheets[0].get("properties").get("sheetId")

    @property
    def names(self):
        names = []
        sheets = self.sheet_metadata.get("sheets", "")
        for sheet in sheets:
            names.append(sheet.get("properties").get("title"))

        return names

    @property
    def sheet(self):
        return self.response

    @property
    def _rows(self):
        return self.sheet["values"]

    def get_rows(self):
        return SheetRows(self.service, self.query, self._rows, self.sheet_id)

    def deep_copy_rows(self):
        rows = []
        for row in self._rows:
            rows.append(row)
        return SheetRows(self.service, self.query, rows, self.sheet_id)
