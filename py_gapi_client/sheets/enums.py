import enum


class DateTimeRenderOption(enum.Enum):
    SERIAL_NUMBER = "SERIAL_NUMBER"
    FORMATTED_STRING = "FORMATTED_STRING"


class ValueInputOption(enum.Enum):
    RAW = "RAW"
    USER_ENTERED = "USER_ENTERED"


class ValueRenderOption(enum.Enum):
    FORMATTED_VALUE = "FORMATTED_VALUE"
    UNFORMATTED_VALUE = "UNFORMATTED_VALUE"
    FORMULA = "FORMULA"
