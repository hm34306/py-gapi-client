import pickle
import os.path
import os
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from .enums import GCredentialType


class GApiTokenNotFound(Exception):
    pass


class CedentialScopeNotProvided(Exception):
    pass


class GApiCredentials:
    def __init__(self, credential_type, credential):
        self.credential_type = credential_type
        self.credential = credential


class ApiToken:
    DEFAULTKEY_FILE_NAME = ".gapi.key"
    GAPI_KEY = "GAPI_KEY"

    @staticmethod
    def get(file=None):
        def get_token(file):
            with open(file, "r") as token:
                return token.read()

        token = None
        if ApiToken.GAPI_KEY in os.environ:
            token = os.environ[ApiToken.GAPI_KEY]
        elif file is not None:
            if not os.path.exists(file):
                raise GApiTokenNotFound(f"Token not found in provided path {file}")
            token = get_token(file)
        elif os.path.exists(f"~/{ApiToken.DEFAULTKEY_FILE_NAME}"):
            token = get_token("~/{ApiToken.DEFAULTKEY_FILE_NAME}")
        elif os.path.exists(ApiToken.DEFAULTKEY_FILE_NAME):
            token = get_token(ApiToken.DEFAULTKEY_FILE_NAME)
        if token is None:
            raise GApiTokenNotFound(
                f"Token not found in environment variable GAPI_TOKEN or files ~/{ApiToken.DEFAULTKEY_FILE_NAME}, {ApiToken.DEFAULTKEY_FILE_NAME}"
            )

        return GApiCredentials(GCredentialType.API_TOKEN, token)


class OAuth2:
    @staticmethod
    def get(scope, file="credentials.json"):
        if scope is None:
            raise CedentialScopeNotProvided(
                "Scope must be provided for oAuth2 credentials"
            )

        creds = None
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(file, scope)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.pickle", "wb") as token:
                pickle.dump(creds, token)

        return GApiCredentials(GCredentialType.OAUTH2, creds)
