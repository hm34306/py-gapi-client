from abc import ABC, abstractproperty
from googleapiclient.discovery import build
from .enums import GCredentialType


class UnableToAuthenticate(Exception):
    pass


class AbstractQuery(ABC):
    def __init__(self, id):
        self.id = id


class AbstractGObject(ABC):
    def __init__(self, service, query, response):
        self.service = service
        self.response = response
        self.query = query


class AbstractGService(ABC):
    def __init__(self, credentials):
        self.credentials = credentials
        self._service = None
        self._build = None

    @abstractproperty
    def service_type(self):
        pass

    @abstractproperty
    def service_version(self):
        pass

    @property
    def service(self):
        if self._service is None:
            if self.credentials.credential_type == GCredentialType.OAUTH2:
                self._service = build(
                    self.service_type,
                    self.service_version,
                    credentials=self.credentials.credential,
                )

            if self.credentials.credential_type == GCredentialType.API_TOKEN:
                self._service = build(
                    self.service_type,
                    self.service_version,
                    developerKey=self.credentials.credential,
                )

            if self.credentials is None:
                raise UnableToAuthenticate(
                    "Google oAuth2 Credintials or api token is required"
                )

        return self._service
