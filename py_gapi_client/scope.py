SCOPE_URI_BASE = "https://www.googleapis.com/auth"


class BaseScope:
    def __init__(self):
        self._scope = []

    def append(self, value):
        if value not in self._scope:
            self._scope.append(value)

    @property
    def scope(self):
        return self._scope


class FluentDriveScope(BaseScope):
    def drive(self):
        self.append(f"{SCOPE_URI_BASE}/drive")
        return self

    def drive_file(self):
        self.append(f"{SCOPE_URI_BASE}/drive.file")
        return self

    def drive_readonly(self):
        self.append(f"{SCOPE_URI_BASE}/drive.readonly")
        return self


class FluentSheetScope(FluentDriveScope):
    def readonly(self):
        self.append(f"{SCOPE_URI_BASE}/spreadsheets.readonly")
        return self

    def full_control(self):
        self.append(f"{SCOPE_URI_BASE}/spreadsheets")
        return self


class FluentDocScope(FluentDriveScope):
    def readonly(self):
        self.append(f"{SCOPE_URI_BASE}/documents.readonly")
        return self

    def full_control(self):
        self.append(f"{SCOPE_URI_BASE}/documents")
        return self
