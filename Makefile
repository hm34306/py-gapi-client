SHELL := /bin/bash

.PHONY: all black build install run style test utest

build:
	poetry build

clean:
	rm -rf py_gapi_client.egg-info && rm -rf dist && rm -rf .pytest_cache

install:
	poetry install

format:
	poetry run black py_gapi_client

style:
	poetry run flake8 py_gapi_client --ignore=E501,E23,W503

utest:
	poetry run pytest test

test: style format utest

run:
	poetry run python -m py_gapi_client

all: install test