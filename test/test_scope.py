import pytest
from py_gapi_client.scope import FluentDriveScope, FluentDocScope, FluentSheetScope


def test_drive_scope():
    drive_scope = FluentDriveScope()
    assert drive_scope.drive().scope == ["https://www.googleapis.com/auth/drive"]


def test_drive_scope_accidentail_multiple_calls():
    drive_scope = FluentDriveScope()
    drive_scope.drive().scope
    drive_scope.drive().scope
    assert drive_scope.drive().scope == ["https://www.googleapis.com/auth/drive"]


def test_drive_scope_combo():
    drive_scope = FluentDriveScope()
    scope = drive_scope.drive().drive_readonly().scope
    assert "https://www.googleapis.com/auth/drive" in scope
    assert "https://www.googleapis.com/auth/drive.readonly" in scope


def test_doc_scope():
    doc_scope = FluentDocScope()
    scope = doc_scope.full_control().scope
    assert "https://www.googleapis.com/auth/documents" in scope


def test_sheet_scope():
    sheet_scope = FluentSheetScope()
    scope = sheet_scope.full_control().scope
    assert "https://www.googleapis.com/auth/spreadsheets" in scope
