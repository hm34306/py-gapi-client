from mock import patch, MagicMock
import pytest
from py_gapi_client import credentials
from py_gapi_client.scope import FluentSheetScope
from google.oauth2.credentials import Credentials


def test_api_key_local():
    assert credentials.ApiToken.get() is not None


def test_api_key_local_type():
    assert credentials.ApiToken.get().credential_type == credentials.GCredentialType.API_TOKEN


def test_api_key_not_found():
    with pytest.raises(credentials.GApiTokenNotFound):
        assert credentials.ApiToken.get("do-not-exist.key") is not None


def test_oauth2_credential_scope_not_provided():
    with pytest.raises(credentials.CedentialScopeNotProvided):
        assert credentials.OAuth2.get(None) is not None


def test_oauth2_credential_scope():
    with patch("py_gapi_client.credentials.OAuth2.get"):
        creds = credentials.OAuth2.get(FluentSheetScope().drive_readonly().scope)
        assert type(creds) is not None

@pytest.mark.skip("requires manual intervention")
def test_manual():
    creds = credentials.OAuth2.get(FluentSheetScope().drive_readonly().scope)
    assert type(creds) is not None